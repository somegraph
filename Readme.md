# TL;DR

This is a possible `D3.js` chart for the recursive dependencies of the
`aptitude` package on Debian stable circa November 2019. See the chart on
`Observable`:
<https://observablehq.com/@coding-exercise/hierarchical-edge-bundling>

# apt-rdepends

To calculate recursive dependencies, I'm using something called
`apt-rdepends`.

`apt-rdepends` man page says:

> By default, apt-rdepends shows a listing of each dependency a package
> has. It will also look at each of these fulfilling packages, and
> recursively lists their dependencies.

[apt-rdepends\_aptitude](./apt-rdepends_aptitude) was generated as
follows:

``` example
apt-rdepends aptitude > apt-rdepends_aptitude
```

# Data massage

[AptRdepends2JSON.py](./AptRdepends2JSON.py) is a one-off script
that goes from `apt-rdepends` output to JSON.

Script uses Python 3.8.0

See docstrings…

# Copy&paste

For the most part, the chart is copy&paste from a `d3.hierarchy`
example… <https://observablehq.com/@d3/hierarchical-edge-bundling>

These are our tweaks…

  - Our data is simpler and thus we need neither the `id` function call in
    `bilink` nor the `hierarchy` function.

  - For browser support, we use the `flatMap` alternative proposed here…
    <https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/flatMap#Alternative>
